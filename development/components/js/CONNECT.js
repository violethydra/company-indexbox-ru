// ============================
//    Name: index.js
// ============================

import AddGeneratorTable from './modules/generatorTable';
import AddGeneratorSearchPanel from './modules/generatorSearchPanel';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddGeneratorTable({
		server: 'units.json',
		selector: '.js__generatorSelect'
	}).run();
	
	new AddGeneratorSearchPanel({
		server: 'products.json',
		selector: '.js__formSearchPanel'
	}).run();
	
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
 