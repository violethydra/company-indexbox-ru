module.exports = class AddLogicFullImage {
	constructor(enter, exit) {
		this.enter = enter;
		this.exit = exit;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		const output = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();

			const clickedFunc = (event) => {
				event.preventDefault();
				document.querySelector('selector').map(item => console.log(item));
				const curElem = event.target;
				const getData = curElem.src || curElem.firstElementChild.src;
				output.style.backgroundImage = `url(${getData})`;
			};

			const mainUrl = [...elem.querySelector('.owl-item').parentElement.children][4];
			const url = mainUrl.querySelector('img').src;
			output.style.backgroundImage = `url(${url})`;
			output.style.width = `${output.parentElement.offsetWidth}px`;

			$(`${this.enter} .gallery__item`).on('click', event => clickedFunc(event));
		}
	}
};
