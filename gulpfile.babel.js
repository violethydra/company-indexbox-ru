// ==================================
// ESLINT-SETTING - don't DEL comment's!!!
// ==================================
/* eslint no-undef: "error" */
/* eslint-env node */
// ==================================

'use strict';

/* ==== IMPORT PARAMS ==== */
import { task, src, dest, series, parallel, watch } from 'gulp';
import log from 'fancy-log';
import path from 'path';
import fs from 'fs';
/* ==== ----- ==== */

/* ==== NODE.JS LISTENER FIX ==== */
import Events from 'events';
Events.defaultMaxListeners = 100;
/* ==== ----- ==== */

/* ==== GULP PLUGIN LOADER ==== */
const iniLoader = {
		rename: {
			'gulp-babel-minify': 'babelMinify',
			'gulp-file-include': 'include',
			'gulp-css-url-adjuster': 'urlReplace',
			'gulp-string-replace': 'htmlReplace',
			'gulp-svg-sprite': 'svgSprite',
			'gulp-svg-symbols': 'svgSymbols'
		}
	},
	_run = require('gulp-load-plugins')(iniLoader),
	isDevelopment = !process.env.NODE_ENV	|| process.env.NODE_ENV === 'development',
	isPublic = process.env.NODE_ENV === 'public';
/* ==== ----- ==== */

/* ==== PLUGIN LIST SEE IN PACKAGE.JSON ==== */
const	combiner = require('stream-combiner2').obj;
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const	inDev = 'development';
const	inDevApps = `${inDev}/components`;
const	inPub = 'public';
/* ==== ----- ==== */

// /* ==== CONFIG ==== */
const __task = nameTask => require(`./gulp_modules/${nameTask}`)(nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig);
// /* ==== ----- ==== */

/* ==== REPLACE ==== */
const	imageClearPath = {
	in: /^development\\components\\/gi,
	out: 'public\\media\\'
};
/* ==== ----- ==== */

/* ==== OPTIONAL ==== */
const	errorConfig = (name, descript, err) => {
		return {
			title: `${name} - ${descript}`,
			message: `Error:\n\n ${err.message}\n`,
			sound: false
		};
	};
const killCache = (name) => {
	let str = name;
		log('filepath origin', name);
		str = str.replace(imageClearPath.in, imageClearPath.out);
		log('filepath result', str);
	fs.unlink(str, (err) => {
		if (err) throw err;
		log('Deleted file: ', path.basename(name));
	});
};
/* ==== ----- ==== */

/* ==== INSTALL PLUGINS ==== */
task('plugins:install:node', __task('plugins-install-node'));
/* ==== MOVE FILES ==== */
task('html:move:pages',      __task('html-move-pages'));
task('html:move:layouts',    __task('html-move-layouts'));
task('html:move:urlreplace', __task('html-move-urlreplace'));
/* ==== BUILD NODE CONTENT ==== */
task('plugins:move:fonts',   __task('plugins-move-fonts'));
task('plugins:move:json',    __task('plugins-move-json'));
task('plugins:move:seo',     __task('plugins-move-seo'));
task('plugins:move:css',     __task('plugins-move-css'));
task('plugins:move:js',      __task('plugins-move-js'));
task('plugins:move:map',     __task('plugins-move-map'));
task('plugins:move:metrica', __task('plugins-move-metrica'));
/* ==== ALL MEDIA GRATHIC ==== */
task('graphic:move:image',   __task('graphic-move-image'));
task('graphic:svg:sprite',   __task('graphic-svg-sprite'));
task('graphic:svg:glyphs',   __task('graphic-svg-glyphs'));
task('graphic:img:compress', __task('graphic-img-compress'));
/* ==== MINIFICATION BUNDLS ==== */
task('files:mini:js',        __task('files-mini-js'));
task('files:mini:css',       __task('files-mini-css'));
task('files:mini:html',       __task('files-mini-html'));
/* ==== STYLUS PARAMS ==== */
task('stylus:setting',       __task('stylus-setting'));
/* ==== WEBPACK PARAMS ==== */
task('webpack:setting',      __task('webpack-setting'));
/* ==== CLEAN PUBLIC FOLDER ==== */
task('files:delete:public',  __task('files-delete-public'));
/* ==== SERVER ==== */
task('server:setting',       __task('server-setting'));
/* ==== EMPTY TASK ==== */
task('skip', cb => cb());
/* ==== ----- ==== */

/* ==== WATCH SETTING ==== */
const __watch = {
	html:    { url: `${inDev}/*.html`, task: 'html:move:pages'},
	img:     { url: `${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`, task: 'graphic:move:image'},
	fonts:   { url: `${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, task: 'plugins:move:fonts'},
	json:    { url: `${inDevApps}/json/**/*.json`, task: 'plugins:move:json'},
	layouts: { url: `${inDev}/layouts/**/*.html`, task: 'html:move:layouts'},
	plugins: { url: `${inDevApps}/plugins/**/*.css`, task: 'plugins:move:css'},
	stylus:  { url:[`${inDevApps}/stylus/**/*.{styl,stylus}`, `${inDev}/tmp/sprite.styl`], task: 'stylus:setting'},
	sprite:  { url: `${inDevApps}/img/__sprite/**/*.svg`, task: 'graphic:svg:sprite'},
	glyphs:  { url: `${inDevApps}/img/__glyphs/**/*.svg`, task: 'graphic:svg:glyphs'},
	pubcss:  { url: `${inPub}/css/**/*.css`, task: 'skip'}
};
/* ==== ----- ==== */

/* ==== WATCH RUN ==== */
task('watchme', () => {
	// * watch Dev
	watch(__watch.html.url,    series(__watch.html.task));
	watch(__watch.layouts.url, series(__watch.layouts.task));
	watch(__watch.stylus.url,  series(__watch.stylus.task));
	watch(__watch.img.url,     series(__watch.img.task)).on('unlink', x => (killCache(x)));
	watch(__watch.fonts.url,   series(__watch.fonts.task));
	watch(__watch.json.url,    series(__watch.json.task));
	watch(__watch.sprite.url,  series(__watch.sprite.task));
	watch(__watch.glyphs.url,  series(__watch.glyphs.task)).on('unlink', x => (killCache(x)));
	watch(__watch.plugins.url, series(__watch.plugins.task));
	// * watch Pub
	watch(__watch.pubcss.url);
});
/* ==== ----- ==== */

/* ==== GULP SETTING ==== */
task('loadingGulpConfig', series(
	parallel('graphic:move:image', 'graphic:svg:sprite', 'graphic:svg:glyphs'),
	parallel('plugins:move:seo', 'plugins:move:fonts', 'plugins:move:json'),
	parallel('html:move:pages', 'html:move:layouts'),
	parallel('html:move:urlreplace'),
	parallel('plugins:install:node', 'stylus:setting'),
	parallel('plugins:move:css', 'plugins:move:map', 'plugins:move:metrica'),
	parallel('webpack:setting')
));
/* ==== ----- ==== */

/* ==== START ==== */
task('default',
	series(
		'files:delete:public',
		'loadingGulpConfig',
		series(isPublic ? parallel('files:mini:css', 'files:mini:js', 'files:mini:html') : series('skip')),
		series(isDevelopment ? parallel('server:setting', 'watchme') : series('skip'))
	));
/* ==== ----- ==== */
