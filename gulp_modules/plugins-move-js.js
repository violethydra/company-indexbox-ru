/* ==== IMPORT PARAMS ==== */
'use strict';

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public',
	inPubJs = `${inPub}/js`;
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(`${inDevApps}/plugins/**/*.js`),
		_run.newer(inPubJs),
		_run.concat('plugins.js'),
		dest(inPubJs)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
