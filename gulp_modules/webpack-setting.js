/* ==== IMPORT PARAMS ==== */

import webpackOrigin from 'webpack';
import webpackStream from 'webpack-stream';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import path from 'path';

/* ==== ----- ==== */


process.traceDeprecation = false;

/* ==== SOURCES AND DIRECTIONS FOR FILES ==== */
const	inDev = 'development';
const	inDevApps = `${inDev}/components`;
const	inPub = 'public';
const	inPubJs = `${inPub}/js`;
const	dir = `${__dirname}\\..\\`;
/* ==== ----- ==== */

/* ==== WEBPACK INIT ==== */
const noDevState = !process.env.NODE_ENV || process.env.NODE_ENV === inDev;
// const { webpack } = webpackStream;
const webpackOriginddCustomPlugins = {
	// picker: path.resolve('./node_modules/pickadate/lib/picker')
};
const PATHS = {
	root: path.join(dir, ''),
	app: path.join(dir, `${inDevApps}/js/`),
	glob: path.join(dir, `${inDevApps}/js/global.js`),
	connected: path.join(dir, `${inDevApps}/js/connect.js`),
	cache: path.resolve(dir, `${inDev}/tmp/cached_uglify/`)
};
/* ==== ----- ==== */

/* ==== OTHER SETTINGS==== */
const __other = {
	compress: {
		warnings: false,
		pure_getters: true,
		unsafe: true,
		unsafe_comps: true,
		screw_ie8: true
	},
	ignorelist: [/\.min\.js$/gi, 'global.js']
};
/* ==== ----- ==== */

/* ==== REPLACE URL OR LINKS ==== */
const __cfg = {
	devtool: noDevState ? 'cheap-module-inline-source-map' : false,
	stats: {
		colors: true,
		modules: true,
		reasons: true,
		errorDetails: true
	},
	babel: [{
		test: /\.js$/,
		exclude: /node_modules/,
		use: {
			loader: 'babel-loader',
			options: { presets: ['@babel/preset-env'] }
		}
	}]
};
/* ==== ----- ==== */

const init = {
	mode: noDevState ? 'development' : 'production',
	entry: {
		connect: PATHS.connected
	},
	output: {
		path: PATHS.root,
		filename: '[name].js',
		sourceMapFilename: '[name].map'
	},
	watch: noDevState === true,
	watchOptions: {
		poll: 1000,
		ignored: /node_modules/
	},
	stats: __cfg.stats,
	devtool: __cfg.devtool,
	module: { rules: __cfg.babel },
	resolve: {
		modules: ['node_modules'],
		extensions: ['.js'],
		alias: webpackOriginddCustomPlugins
	},
	performance: {
		hints: 'error'
	},
	optimization: {
		occurrenceOrder: true,
		minimizer: [
			new UglifyJsPlugin({
				cache: PATHS.cache,
				sourceMap: true,
				parallel: true,
				exclude: __other.ignorelist
			})
		],
		splitChunks: {
			cacheGroups: {
				query: {
					test: /jquery/,
					name: 'plugins',
					chunks: 'initial',
					minSize: 1,
					priority: 21,
					reuseExistingChunk: true
				},
				owlcarousel: {
					test: /owl\.carousel/,
					name: 'plugins',
					chunks: 'initial',
					minSize: 1,
					priority: 11,
					reuseExistingChunk: true
				},
				vendor: {
					name: 'plugins',
					chunks: 'all',
					test: /node_modules/,
					priority: 20
				},
				connect: {
					name: 'connect',
					minChunks: 2,
					chunks: 'async',
					priority: 10,
					reuseExistingChunk: true,
					enforce: true
				}
			}
		}
	},
	plugins: [
		// new webpack.ProvidePlugin({
		// 	$: 'jquery',
		// 	jQuery: 'jquery',
		// 	'window.jQuery': 'jquery'
		// })
	]
};

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>

	(flag, firstRun = true) => (combiner(
		src(`${inDevApps}/js/**/*.js`), webpackStream(init, webpackOrigin),
		dest(inPubJs).on('data', () => firstRun ? flag() : null || '')
	)).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
