/* ==== IMPORT PARAMS ==== */

'use strict';

import resolver from 'stylus';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public',
	inPubCss = `${inPub}/css`;
/* ==== ----- ==== */
 
 
/* ==== Replace URL or Links ==== */
const __cfg = {
	autoprefixer: {
		dev: {
			grid: false,
			cascade: false,
			flexbox: false,
			remove: true,
			browsers: ['defaults']
		},
		pub: {
			grid: true,
			cascade: true,
			flexbox: true,
			remove: false,
			browsers: ['last 5 versions', 'Firefox ESR', 'not dead', 'ie 6-11']
		}
	},
	stylus: {
		sprite: {
			import: `${__dirname}/../${inDev}/tmp/sprite`,
			define: { url: resolver() }
		}
	}
};
/* ==== ----- ==== */

/* ==== Replace URL or Links ==== */
const root = { a: '..' };
const __link = {
	css: {
		image: {
			prepend: `${root.a}/media/img/`
		},
		fonts: {
			replace: [`${root.a}/media/img/fonts/`, `${root.a}/media/fonts/`]
		}
	}
};
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDevApps}/stylus/**/connect.styl`),
		_run.if(isDevelopment, _run.sourcemaps.init()), 
		_run.changed(inPubCss, { extension: '.css' }),
		_run.stylus(__cfg.stylus.sprite),
		_run.if(isDevelopment, _run.autoprefixer(__cfg.autoprefixer.dev)),
		_run.if(isPublic, _run.autoprefixer(__cfg.autoprefixer.pub)),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace(__link.css.image),
		_run.urlReplace(__link.css.fonts),
		_run.rename('global.css'),
		dest(inPubCss))
	.on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
